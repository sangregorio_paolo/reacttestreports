import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { BrowserRouter } from 'react-router-dom'

import { createStore } from 'redux';
import { combineReducers } from 'redux';

import reportsListing from './components/reports-listing/reducer';
import report from './components/report/reducer';

const initialState = {
    reportsListing: {
        reports: [],
        billingPeriod: null
    },
    report: {
        columns: [],
        data: []
    }
};

const rootReducer = combineReducers({ reportsListing, report });

const store = createStore(rootReducer, initialState);

ReactDOM.render(<BrowserRouter><App store={store} /></BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
