export const reportListReceived = (content) => {
    return {
        type: "REPORTS_FETCH_COMPLETED",
        reports: content
    }
}

export const fetchReportsList = (dispatch) => {
    fetch("/api/reports.json")
        .then(res => res.json())
        .then(obj => dispatch(reportListReceived(obj.content)))
}
