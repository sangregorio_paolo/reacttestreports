import React, {Component} from 'react'
import {connect} from 'react-redux'
import { fetchReportsList } from './actions'
import { Table, Grid, Row, Col, Button } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

class ReportListingRow extends React.Component {
  render() {
    return (
      <tr>
        <td>{this.props.report.name}</td>
        <td>{this.props.report.description}</td>
        <td><input type="button" value="Run" onClick={() => this.props.onRun(this.props.report.id)} /></td>
      </tr>
    )
  }
}

class ReportsListing extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchReports();
  }

  runReport(reportId) {
    this.props.history.push(`/report/${reportId}/${this.monthSelect.value}`);
  }

  buttonFormatter(cell, row){
    return (
      <Button onClick={() => { this.runReport.bind(this)(row.id)}} bsStyle="success">Run</Button>
    )
  }

  render() {
    let data = this.props.reports.map((el) => { return { id: el.id, name: el.name, description: el.description } });

    return (
      <Grid>
        <Row className="show-grid">
          <Col md={12}>
            <select ref={(node) => this.monthSelect = node}>
              <option value="201708">Aug 2017</option>
              <option value="201709">Sep 2017</option>
              <option value="201710">Oct 2017</option>
            </select>
            <BootstrapTable striped bordered condensed hover data={data}>
              <TableHeaderColumn dataField="id" isKey={true} dataAlign="center" dataSort={true} hidden>Product ID</TableHeaderColumn>
              <TableHeaderColumn dataField="name" dataSort={true}>Name</TableHeaderColumn>
              <TableHeaderColumn dataField="description" dataSort={true}>Description</TableHeaderColumn>
              <TableHeaderColumn dataField="button" dataFormat={this.buttonFormatter.bind(this)}></TableHeaderColumn>
            </BootstrapTable>
          </Col>
        </Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    reports: state.reportsListing.reports,
    isLoading: state.reportsListing.isLoading
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    fetchReports: () => {
      fetchReportsList(dispatch)
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReportsListing);
