export default (state, action) => {
    switch(action.type) {
        case "REPORTS_FETCH_STARTED":
            return {
                ...state,
                reports: [],
                isLoading: true
            }
        case "REPORTS_FETCH_COMPLETED":
            return {
                ...state,
                reports: action.reports,
                isLoading: false
            }
        case "FETCH_DATA_FAILED":
            return {
                ...state
            }
        default:
            return {
                reports: [],
                billingPeriod: null,
                isLoading: false
            }
    }
};
