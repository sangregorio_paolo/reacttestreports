export default (state, action) => {
    switch(action.type) {
        case "REPORT_FETCH_COMPLETED":
            return {
                ...state,
                columns: action.columns,
                data: action.data
            };
        default:
            return { columns: [], data: [] }
    }
}
