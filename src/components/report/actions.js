export const reportReceived = (content) => {
    return {
        type: "REPORT_FETCH_COMPLETED",
        columns: content.columns,
        data: content.data
    }
}


export const reportFailed = () => {
    return {
        type: "REPORT_FETCH_COMPLETED",
        columns: [],
        data: []
    }
}


export const getReport = (dispatch, id, period) => {
    console.log("Requesting");
    console.log(`/api/reports/${id}-${period}.json`);
    fetch(`/api/reports/${id}-${period}.json`)
        .then(handleErrors)
        .then(res => res.json())
        .then(obj => dispatch(reportReceived(obj)))
        .catch(error => dispatch(reportFailed()))
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
