import React, {Component} from 'react'
import {connect} from 'react-redux'

import {getReport, reportReceived} from './actions'

import { Table, Grid, Row, Col } from 'react-bootstrap';


class TableHeaderCell extends React.Component {
  render() {
    return(
      <th>{this.props.name}</th>
    )
  }
}

class TableRow extends React.Component {
  render() {
    return(
      <tr>
        {this.props.values.map((value, i) => {
          return (
            <TableRowCell value={value} key={i} />
          )
        })}
      </tr>
    )
  }
}

class TableRowCell extends React.Component {
  render() {
    return(
      <td>{this.props.value}</td>
    )
  }
}


class Report extends React.Component {
  componentDidMount() {
    this.props.getReport(this.props.match.params.id, this.props.match.params.period);
  }

  render() {
    return (
      <Grid>
        <Row className="show-grid">
          <Col md={12}>
            <Table striped bordered condensed hover>
              <thead>
                <tr>
                  {this.props.columns.map((column, i) => {
                    return (
                      <TableHeaderCell name={column} key={i} />
                    )
                  })}
                </tr>
              </thead>
              <tbody>
                {this.props.data.map((row_data, i) => {
                  return (
                    <TableRow values={row_data} key={i} />
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    columns: state.report.columns,
    data: state.report.data
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    getReport: (id, period) => {
      getReport(dispatch, id, period)
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Report);
