import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ReportsListing from './components/reports-listing/reportsListingComponent'
import Report from './components/report/report-component'
import { Provider } from 'react-redux'
import { Switch, Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <div className="App">
          <Switch>
            <Route exact path='/' component={ReportsListing}/>
            <Route path='/report/:id/:period' component={Report}/>
          </Switch>
        </div>
      </Provider>
    );
  }
}

export default App;
